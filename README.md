# Ansible Playbooks for @CodeLenny's Machines

## PolarLizard

PolarLizard is the codename for an Ubuntu 16.04 LTS installation on Ryan's ThinkPad W550s.

See @CodeLenny's Backup/Installation document for the entire history and manual machine setup.

### `chroot` Installation

When it's time to setup the PolarLizard BTRFS subvolume inside a chroot, run:

```bash
sudo ansible-playbook -i chroot polar_lizard.yml -f 1
```

### Internal Updates

To update PolarLizard from inside a running machine, run:

```bash
sudo ansible-playbook -i local polar_lizard.yml -f 1
```

## InconceivableKoala

InconceivableKoala is a "V2" of PolarLizard, but was manually bootstrapped to get things off the ground faster.

### Installation

A `Storage` BTRFS volume is assumed.  Start the standard Ubuntu (16.04) installation from a LiveCD (or live partition),
and choose the volume as the root partition.  (Make sure "reformat" remains unchecked!)
Also choose the designated swap partition.

Once installation has finished, the permissions on the `Storage` volume will have been changed.

While still in the live distro, update the partition:

```bash
sudo chmod a+rwx /media/ubuntu/Storage
```

(You may reboot to ensure that Refind still locates the partition.)

You may need to adjust the default bootloader back to Refind, the installation can change it the Grub installation called `ubuntu`.
In the Lenovo BIOS, the correct selection was the name of the hard drive.

Now move the new `@` and `@home` BTRFS subvolumes into the proper locations.

```bash
cd /media/ubuntu/Storage
btrfs subvolume create @inconceivable_koala
sudo mv @     @inconceivable_koala/root
sudo mv @home @inconceivable_koala/home
```

(You may reboot to ensure that Refind )